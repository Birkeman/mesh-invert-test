﻿using UnityEngine;
using System.Collections;

public class MeshTest : MonoBehaviour
{

	// Here I invert the mesh for the collider
	void Awake ()
	{
		MeshCollider mc = GetComponent<MeshCollider>();
		Mesh mesh = mc.sharedMesh;
		Mesh newMesh = new Mesh();

		int[] triangles = mesh.triangles;
		int[] newTris = new int[triangles.Length];

		newMesh.vertices = mesh.vertices;
		newMesh.uv = mesh.uv;

		for(int i = triangles.Length - 1; i >= 0; i--)
		{
			newTris[triangles.Length - 1 - i] = triangles[i];
		}

		newMesh.triangles = newTris;
		mc.sharedMesh = newMesh;
	}
	
	//and here I 
	void Start()
	{
		Vector3 origin = Vector3.up * 10;
		Vector3 direction = Vector3.down;
		
		if(Physics.Raycast(origin, direction))
		{
			print("It works!");
		}
 	}

	// Update is called once per frame
	void Update () {
	
	}
}
